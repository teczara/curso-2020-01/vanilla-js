import { UsuariosService } from '../../js/services/usuarios.service.js';

class NuevoUsuarioPage {

    constructor(usuariosService){
        this.usuariosService = usuariosService;
    }

    inicializar() {
        const btnGuardar = document.getElementById('guardar');
        btnGuardar.addEventListener('click', () => this.onGuardar());
    }

    onGuardar() {
        const form = document.getElementById('frm-usuario');
        const nuevoUsuario = {
            name: form.elements['name'].value,
            email: form.elements['email'].value,
            phone: form.elements['phone'].value,
            website: form.elements['website'].value,
            company: {
                name: form.elements['company'].value
            }
        }
        this.usuariosService.addUsuario(nuevoUsuario).then(respuesta => {
            alert('Se guardó');
        });
    }
}

window.onload = () => {
    const usuariosService = new UsuariosService();
    const page = new NuevoUsuarioPage(usuariosService);
    page.inicializar()
}