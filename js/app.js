import { UsuariosService } from './services/usuarios.service.js'

class App {

    apiUrl = 'http://localhost:3000';
    usuariosService = null;

    constructor(usuariosService) {
        this.usuariosService = usuariosService;
    }

    inicializar() {

        this.loadUsuarios();

        const boton = document.querySelector('#refresh');
        boton.addEventListener('click', () => {
            this.loadUsuarios();
        });

    }

    loadUsuarios() {
        document.getElementById('loading').innerHTML = 'Cargando...';
        this.usuariosService.getUsuarios().then(
            (json) => this.renderUsuarios(json)
        );
    }

    addUsuario(usuario) {
        this.usuariosService.addUsuario(usuario).then(
            (respuesta) => {
                alert(respuesta.mensaje);
                this.loadUsuarios();
            }
        );
    }

    renderUsuarios(usuarios) {
        document.getElementById('loading').innerHTML = '';
        const content = document.querySelector('main');
        content.innerHTML = '';
        usuarios.forEach(usuario => {
            const div = document.createElement('div');
            div.innerHTML = `
                <div class="card">
                    <div><strong>${usuario.name}</strong></div>
                    <div>
                        <p>${usuario.email}</p>
                        <p>${usuario.phone}</p>
                        <p><a href="${usuario.website}">${usuario.website}</a></p>
                        <p>${usuario.company.name}</p>
                    </div>
                </div>
            `;
            content.appendChild(div);
        });
    }
}

window.onload = () => {
    const usuariosService = new UsuariosService();
    const app = new App(usuariosService);
    app.inicializar();
};