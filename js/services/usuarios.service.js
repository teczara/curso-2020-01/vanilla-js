export class UsuariosService {

    apiUrl = 'http://localhost:3000';

    getUsuarios() {
        //GET
        return fetch(this.apiUrl + '/usuarios') //Promise
          .then((response) => response.json()) //Promise
    }

    addUsuario(usuario) {
        //POST
        return fetch(this.apiUrl + '/usuarios', {
            method: 'POST',
            body: JSON.stringify(usuario),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => response.json());
    }
}