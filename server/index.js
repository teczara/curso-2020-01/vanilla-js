const express = require('express');
const cors = require('cors');

const app = express();

const usuarios = [
    {
      "id": 1,
      "name": "Juan Pereasdadz",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    }
  ];

app.use(cors());

app.use(express.json());

app.get('/', (request, response) => {
    console.log('alguien intenta acceder al server');
    const params = request.query;
    console.log(request.query.nombre);
    response.send(`Hola ${params.nombre}!!!`);
});

app.get('/usuarios', (req, res) => {
    res.send(usuarios);
});

app.post('/usuarios', (req, res) => {
  usuarios.push(req.body);
  res.send({mensaje: 'Usuario Guadado'});
});

app.listen(3000, () => {
    console.log('Servidor iniciado');
});